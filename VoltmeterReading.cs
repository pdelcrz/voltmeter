using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum TerminalSetting { V_3 = 3, V_15 = 15, V_300 = 300};

public class VoltmeterReading : MonoBehaviour
{
    public GameObject pivot;

    //Stores the rotation of the pivot
    private Quaternion startRotation;
    private Quaternion currentRotation; 

    //Conditions
    public bool emptyConnection = true;
    public bool terminal3Connection = false;
    public bool terminal15Connection = false;
    public bool terminal300Connection = false;
    public bool negativeConnection = false;

    //Value storing voltage
    private float voltage;

    // Start is called before the first frame update
    void Start()
    {
        //Store the current rotation of the needle
        startRotation = pivot.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        //Store the current position of the needle
        currentRotation = pivot.transform.rotation;
        
        //check if negative terminal and a positive terminal is selected
        CheckConnection();

        if (emptyConnection == false)
        {
            //Store voltage - Currently using for Testing purposes
            SetVoltage(3);
            
            // Terminal 3 
            if (terminal3Connection == true && negativeConnection == true)
            {
                MeasureVoltage(TerminalSetting.V_3);
            }

            // Terminal 15
            if (terminal15Connection == true && negativeConnection == true)
            {
                MeasureVoltage(TerminalSetting.V_15);
            }

            // Terminal 300
            if (terminal300Connection == true && negativeConnection == true)
            {
                //RotateTerminal300();
                MeasureVoltage(TerminalSetting.V_300);
            }
        }
        
        //Return to the original position when there is no available connection
        if (emptyConnection == true && currentRotation != startRotation)
        {
            Debug.Log("No terminals connected");
            ReturnToHomePosition();
        }
    }

    /// <summary>
    /// This function checks the connection at the terminal ports. The function checks the negative terminal first before the positive terminals
    /// </summary>
    void CheckConnection()
    {
        if (negativeConnection == true)
        {
            //Check if a terminal has been selected
            if (terminal3Connection == true || terminal15Connection == true || terminal300Connection == true)
            {
                emptyConnection = false;
            }
        }

        //If the negative terminal is disconnected, the voltmeter will read no connection 
        if (negativeConnection == false)
        {
            emptyConnection = true;
        }
    }

    public void SetVoltage(float volt)
    {
        voltage = volt;
    }

    /// <summary>
    /// This function takes in the setting passed in from the update function to rotate the needle based the terminal connected.
    /// </summary>
    /// <param name="setting"></param>
    void MeasureVoltage(TerminalSetting setting)
    {
        float yAngles = 0 + voltage * (110.0f / (int) setting );
        iTween.RotateTo(pivot.gameObject, new Vector3(pivot.transform.localEulerAngles.x, yAngles, pivot.transform.localEulerAngles.z), 2);
    }

    private void ReturnToHomePosition()
    {
        // return to originial position when not in use
        pivot.transform.rotation = startRotation;
        Debug.Log("Returning to origin position");
    }
}
